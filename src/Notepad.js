import React, {useState, useRef, useEffect} from 'react';

export default function Notepad({id, note, handleNoteUpdated}) {
  const [isSaving, setSaving] = useState(false);
  const [noteRef, setNoteRef] = useState(note.value)
  let currentNote = '';

  useEffect(() => {
    const interval = setInterval(() => {
      if (currentNote !== noteRef) {
        setSaving(true);
        currentNote = noteRef;
        setTimeout(() => {
          setSaving(false);
          handleNoteUpdated(currentNote);
        }, 1000);
      }
    }, 2000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <textarea cols="50" rows="10" value={noteRef}></textarea>
      <p>{isSaving ? 'Saving...' : 'Saved'}</p>
    </>
  );
}
