import React, {useState, useEffect} from 'react';
import NotesList from './NotesList';
import Notepad from './Notepad';
import {v4 as uuid} from 'uuid';

function App() {
  const [selectedNote, setSelectedNote] = useState({});
  const [allNotes, setAllNotes] = useState([]);

  useEffect(() => {
    setAllNotes(JSON.parse(localStorage.getItem('notes')) || []);
  }, [])

  function handleNoteOpened(id) {
    setSelectedNote(allNotes.find(x => x.id === id));
  }

  function handleCreateNewNote() {
    const newNote = {id: uuid(), title: '', content: ''};
    const newNotes = [...allNotes, newNote];

    setAllNotes(newNotes);
    setSelectedNote(newNote);
    localStorage.setItem('notes', JSON.stringify(newNotes));
  }

  function handleNoteUpdated(val) {
    console.log(selectedNote, val);
    // const index = allNotes.findIndex(x => x.id === selectedNote.id);
    // console.log(index);
    // const newNotes = [...allNotes];
    // newNotes[index].content = val;
    // setAllNotes(newNotes);
    // localStorage.setItem('notes', JSON.stringify(newNotes));
  }

  return (
    <>
      <NotesList handleNoteOpened={handleNoteOpened} handleCreateNewNote={handleCreateNewNote} list={allNotes} />
      <Notepad note={selectedNote} handleNoteUpdated={handleNoteUpdated} />
    </>
  );
}

export default App;
