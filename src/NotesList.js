import React from 'react';

export default function NotesList({list, handleNoteOpened, handleCreateNewNote}) {
  function openNote(id) {
    handleNoteOpened(id);
  }

  function createNewNote() {
    handleCreateNewNote();
  }

  return (
    <>
      {JSON.stringify(list)}
      <ul>
        {list.map(note => {return <li key={note.id} onClick={openNote}>{note.id}</li>})}
      </ul>
      <button onClick={createNewNote}>Create New Note</button>
    </>
  )
}
